import 'dart:async';
import 'package:rxdart/rxdart.dart';

class MatrixBloc {
  BehaviorSubject<List> _MatrixCollection = BehaviorSubject<List>();
  BehaviorSubject<int> _NumeroIskasCollection = BehaviorSubject<int>();

  //variables del blox
  int ta = 10;
  int tb = 14;
  int numeroIslas = 0;
  List matrix = [
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
  ];

  //funciones
  void modificarCampo(int x, int y) {
    if (matrix[x][y] == '1' || matrix[x][y] == 'x') {
      matrix[x][y] = '0';
    } else if (matrix[x][y] == '0') {
      matrix[x][y] = '1';
    }
    _MatrixCollection.sink.add(matrix);
    validadNumeroIslas();
  }

  void validadNumeroIslas() {
    numeroIslas = 0;
    reiniciarMatrix();
    for (int i = 0; i < tb; i++) {
      for (int j = 0; j < ta; j++) {
        if (matrix[i][j] == '1') {
          matrix[i][j] = 'x';
          matrix = volverXHorizontal(matrix, j, i);
          matrix = volverXVertical(matrix, j, i);
          numeroIslas++;
        }
      }
    }
    _NumeroIskasCollection.sink.add(numeroIslas);
  }

  reiniciarMatrix() {
    for (int i = 0; i < tb; i++) {
      for (int j = 0; j < ta; j++) {
        if (matrix[i][j] == 'x') {
          matrix[i][j] = '1';
        }
      }
    }
  }

  mostrarMatrix(List a) {
    for (int i = 0; i < a.length; i++) {
      print('${a[i]}');
    }
  }

  List volverXHorizontal(List a, int x, int y) {
    for (int i = x; i < ta; i++) {
      if (a[y][i] == '1') {
        a[y][i] = 'x';
        a = volverXVertical(a, i, y);
        a = volverVerticalreversa(a, i, y);
      } else if (a[y][i] == '0') {
        return a;
      }
    }
    return a;
  }

  List volverHorizontalreversa(List a, int x, int y) {
    for (int i = x; i >= 0; i--) {
      if (a[y][i] == '1') {
        a[y][i] = 'x';
        a = volverXVertical(a, i, y);
      } else if (a[y][i] == '0') {
        return a;
      }
    }
    return a;
  }

  List volverVerticalreversa(List a, int x, int y) {
    for (int i = y; i >= 0; i--) {
      if (a[i][x] == '1') {
        a[i][x] = 'x';
        a = volverXHorizontal(a, x, i);
      } else if (a[i][x] == '0') {
        return a;
      }
    }
    return a;
  }

  List volverXVertical(List a, int x, int y) {
    for (int i = y; i < tb; i++) {
      if (a[i][x] == '1') {
        a[i][x] = 'x';
        a = volverXHorizontal(a, x, i);
        a = volverHorizontalreversa(a, x, i);
      } else if (a[i][x] == '0') {
        return a;
      }
    }
    return a;
  }

  //recuperar data
  Stream<List> get MatrixData => _MatrixCollection.stream;
  Stream<int> get NumeroIslasData => _NumeroIskasCollection.stream;

  //constructor
  MatrixBloc() {
    _MatrixCollection.sink.add(matrix);
    _NumeroIskasCollection.sink.add(numeroIslas);
    validadNumeroIslas();
  }

  dispose() {
    _MatrixCollection.close;
  }
}
