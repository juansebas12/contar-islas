import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:prueba/matrix_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Maps',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Maps'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  late MatrixBloc matrixBloc = new MatrixBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            StreamBuilder(
                stream: matrixBloc.MatrixData,
                builder: (context, snapshop) {
                  return Column(
                    children: [
                      ..._colums(),
                    ],
                  );
                }),
            StreamBuilder<int>(
                stream: matrixBloc.NumeroIslasData,
                builder: (context, snapshot) {
                  return Container(
                    margin: EdgeInsets.only(top: 16),
                    child: Center(
                      child: Text('Cantidad de islas = ${snapshot.data}'),
                    ),
                  );
                })
          ],
        ));
  }

  List<Widget> _colums() {
    List<Widget> matrix = [];
    for (var i = 0; i < matrixBloc.tb; i++) {
      matrix.add(new Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: _rows(i),
      ));
    }
    return matrix;
  }

  List<Widget> _rows(int index) {
    List<Widget> matrix = [];
    for (var i = 0; i < matrixBloc.ta; i++) {
      matrix.add(InkWell(
          onTap: () {
            matrixBloc.modificarCampo(index, i);
          },
          child: button(matrixBloc.matrix[index][i])));
    }
    return matrix;
  }

  Widget button(String text) {
    return Container(
      width: 30,
      height: 30,
      color: text == '1' || text == 'x' ? Colors.green : Colors.blue,
      child: Center(
        child: Text(text == '1' || text == 'x' ? '1' : '0'),
      ),
    );
  }
}
